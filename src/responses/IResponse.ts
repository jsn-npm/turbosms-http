import {ResponseCode} from './ResponseCode';
import {ResponseStatus} from './ResponseStatus';

export interface IResponse<T extends any = any>{

    response_code: ResponseCode;
    response_status: ResponseStatus;
    response_result: T|null;

}
