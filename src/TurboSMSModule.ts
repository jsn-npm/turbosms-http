import {TurboSMS} from './TurboSMS';
import {AxiosInstance} from 'axios';

export abstract class TurboSMSModule{

    protected readonly turboSMS: TurboSMS;
    protected readonly httpClient: AxiosInstance;

    /**
     * @param {TurboSMS} turboSMSClient
     * @param {AxiosInstance} httpClient
     */
    public constructor(turboSMSClient: TurboSMS, httpClient: AxiosInstance) {
        this.turboSMS = turboSMSClient;
        this.httpClient = httpClient;
    }

}
