export enum MessageStatus{

    Queued = 'Queued',
    Accepted = 'Accepted',
    Sent = 'Sent',
    Delivered = 'Delivered',
    Read = 'Read',
    Expired = 'Expired',
    Undelivered = 'Undelivered',
    Rejected = 'Rejected',
    Unknown = 'Unknown',
    Failed = 'Failed',
    Cancelled = 'Cancelled'

}
