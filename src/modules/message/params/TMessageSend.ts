type TMessageSendCommonParams = {
    sender: string;
    start_time?: string; // yyyy-mm-dd HH:ii
    text?: string;
    recipients: string[];
};

type TMessageSendSMSParams = {
    is_flash?: boolean;
};

type TMessageSendViberParams = {
    ttl?: number;
    image_url?: string;
    caption?: string;
    action?: string;
    file_id?: number;
    count_clicks?: boolean;
    is_transactional?: boolean;
};

type TMessageSendSMS = TMessageSendCommonParams & TMessageSendSMSParams & {
    sms: Partial<TMessageSendCommonParams> & TMessageSendSMSParams;
};

type TMessageSendViber = TMessageSendCommonParams & TMessageSendViberParams & {
    viber: Partial<TMessageSendCommonParams> & TMessageSendViberParams;
};

type TMessageSendBoth = TMessageSendCommonParams & TMessageSendSMSParams & TMessageSendViberParams & {
    sms: Partial<TMessageSendCommonParams> & TMessageSendSMSParams;
    viber: Partial<TMessageSendCommonParams> & TMessageSendViberParams;
};

export type TMessageSend = TMessageSendSMS | TMessageSendViber | TMessageSendBoth;
