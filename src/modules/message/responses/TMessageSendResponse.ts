import {ResponseCode, ResponseStatus} from '../../../responses';

export type TMessageSendResponse = null|{

    phone: string;
    message_id: null|string;
    response_code: ResponseCode;
    response_status: ResponseStatus;

}[];
