import {ResponseCode, ResponseStatus} from '../../../responses';
import {DateTime} from 'luxon';
import {MessageStatus} from '../MessageStatus';
import {RejectedStatus} from '../RejectedStatus';

type CommonFields = {
    message_id: string;
    response_code: ResponseCode;
    response_status: ResponseStatus;
};

type AdditionalFields = {
    recipient: string;
    sent: null|DateTime;
    updated: DateTime;
    status: MessageStatus;
};

type SMSFields = {
    type: 'sms';
};

type ViberFields = {
    type: 'viber';
    rejected_status: RejectedStatus;
    click_time: null|DateTime;
};

type OkResponse = {type: 'sms' | 'viber'; } & CommonFields & AdditionalFields & (SMSFields | ViberFields);
type ErrResponse = CommonFields;

export type TMessageStatusResponse = (ErrResponse | OkResponse)[];
