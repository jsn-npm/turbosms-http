import {TurboSMSModule} from '../../TurboSMSModule';
import {TMessageSend} from './params';
import {IResponse} from '../../responses';
import {TMessageSendResponse, TMessageStatusResponse} from './responses';
import {DateTime} from 'luxon';

export class Message extends TurboSMSModule{

    /**
     * @param {TMessageSend} params
     * @return {Promise<IResponse<TMessageSendResponse>>}
     */
    public async send(params: TMessageSend): Promise<IResponse<TMessageSendResponse>>{
        const data: any = {
            sender: params.sender,
            recipients: params.recipients
        };

        if('sms' in params)
            data.sms = params.sms;

        if('viber' in params)
            data.viber = params.viber;

        const response = await this.httpClient.post('/message/send.json', data);
        if(response.status === 200)
            return response.data as IResponse<TMessageSendResponse>;

        throw new Error('Failed to send message');
    }

    /**
     * @param {string} message_id
     * @return {Promise<IResponse<TMessageStatusResponse>>}
     */
    public async status(message_id: string): Promise<IResponse<TMessageStatusResponse>>;
    /**
     * @param {string[]} message_id
     * @return {Promise<IResponse<TMessageStatusResponse>>}
     */
    public async status(message_id: string[]): Promise<IResponse<TMessageStatusResponse>>;
    public async status(message_id: string|string[]): Promise<IResponse<TMessageStatusResponse>>{
        if(!Array.isArray(message_id))
            message_id = [message_id];

        const result = await this.httpClient.post('/message/status.json', {
            messages: message_id
        });

        if(result.status === 200){
            if(result.data.response_result !== null && Array.isArray(result.data.response_result)){
                result.data.response_result = result.data.response_result.map((entry: any) => {
                    if(entry?.sent)
                        entry.sent = DateTime.fromFormat('yyyy-LL-dd HH:ii:ss', entry.sent);

                    if(entry?.updated)
                        entry.updated = DateTime.fromFormat('yyyy-LL-dd HH:ii:ss', entry.updated);

                    if(entry?.click_time)
                        entry.click_time = DateTime.fromFormat('yyyy-LL-dd HH:ii:ss', entry.click_time);

                    return entry;
                });
            }

            return result.data as IResponse<TMessageStatusResponse>;
        }

        throw new Error('Failed to acquire status of messages');
    }

}
