import {TurboSMSModule} from '../../TurboSMSModule';
import {TFileAdd} from './TFileAdd';
import {IResponse} from '../../responses';
import {TFileDetailsResponse} from './TFileDetailsResponse';

export class File extends TurboSMSModule{

    /**
     * @param {TFileAdd} params
     * @return {Promise<IResponse<TFileDetailsResponse>>}
     */
    public async add(params: TFileAdd): Promise<IResponse<TFileDetailsResponse>>{
        const result = await this.httpClient.post('/file/add.json', {
            ...params
        });

        if(result.status === 200)
            return result.data as IResponse<TFileDetailsResponse>;

        throw new Error('Failed to add file');
    }

    /**
     * @param {number} id
     * @return {Promise<IResponse<TFileDetailsResponse>>}
     */
    public async details(id: number): Promise<IResponse<TFileDetailsResponse>>{
        const result = await this.httpClient.post('/file/details.json', {
            id: id
        });

        if(result.status === 200)
            return result.data as IResponse<TFileDetailsResponse>;

        throw new Error('Failed to get file details');
    }

}
