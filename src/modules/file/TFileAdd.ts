type TFileAddByUrl = {
    url: string;
};

type TFileAddByData = {
    data: string;
};

export type TFileAdd = TFileAddByUrl | TFileAddByData;
