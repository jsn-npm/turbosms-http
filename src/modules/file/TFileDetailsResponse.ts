import {DateTime} from 'luxon';

export type TFileDetailsResponse = null|{
    id: number;
    link: string;
    source: string;
    size: number;
    created: DateTime;
};
