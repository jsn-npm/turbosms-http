import {TurboSMSModule} from '../../TurboSMSModule';
import {IResponse} from '../../responses';
import {TBalanceResponse} from './TBalanceResponse';

export class User extends TurboSMSModule{

    /**
     * @return {Promise<IResponse<TBalanceResponse>>}
     */
    public async balance(): Promise<IResponse<TBalanceResponse>>{
        const response = await this.httpClient.post('/user/balance.json');
        if(response.status === 200)
            return response.data as IResponse<TBalanceResponse>;

        throw new Error('Failed to acquire user balance');
    }

}
