export type TBalanceResponse = {
    balance: number;
};
