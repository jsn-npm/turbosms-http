import axios, {AxiosInstance} from 'axios';
import {ITurboSMSParams} from './ITurboSMSParams';
import {Message, User, File} from './modules';

export class TurboSMS{

    protected readonly params: ITurboSMSParams;
    protected readonly httpClient: AxiosInstance;

    /**
     * @param {ITurboSMSParams} params
     */
    public constructor(params: ITurboSMSParams) {
        this.params = params;
        this.httpClient = axios.create({
            baseURL: 'https://api.turbosms.ua/',
            maxRedirects: 3,
            decompress: true,
            withCredentials: true,
            headers: {
                'Authorization': `Bearer ${this.params.accessToken}`
            }
        });
    }

    /**
     * @return {Message}
     */
    public get message(): Message{
        if(this.message_instance === null)
            this.message_instance = new Message(this, this.httpClient);

        return this.message_instance;
    }
    private message_instance: Message|null = null;

    /**
     * @return {User}
     */
    public get user(): User{
        if(this.user_instance === null)
            this.user_instance = new User(this, this.httpClient);

        return this.user_instance;
    }
    private user_instance: User|null = null;

    /**
     * @return {File}
     */
    public get file(): File{
        if(this.file_instance === null)
            this.file_instance = new File(this, this.httpClient);

        return this.file_instance;
    }
    private file_instance: File|null = null;

}
