## How to use
1. Firstly, you need to enable HTTP API on the TurboSMS site
2. Then you need to copy token
3. Finally, you can use this library (you should install it first of course `npm i -S turbosms-http`)
```typescript
import {TurboSMS} from 'turbosms-http';
const turboSMS = new TurboSMS({
    accessToken: 'your-token-obtained-from-TurboSMS-site'
});
```

You can get information about shape of a response data here: https://turbosms.ua/api.html

### Methods
* `user.balance()`
* `message.send()`
* `message.status(message_id:string|string[])`
* `file.add()`
* `file.details()`

I'll describe all the methods above later, now I'm too lazy to do it 
